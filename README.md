# LOL Security Fun

"We're not happy until you're not happy."
--Security, according to Dave Anselmi

## Why Bother?

- Assets and Reputation

## CIA

## Email

- Spam: UCE/UBE
  * unsubscribe when it's persistent and reputable
  * block when it's persistent and trashy
  * otherwise, just delete
  * *never* do business with spammers

- Attacks: Phishing, whaling, hoaxes, viruses, and so on
  * If an email makes you excited, angry, or fearful, it's probably bullshit.
  * ... even if it seems to be coming from your boss, friend, or co-worker
  * delete it, just delete it
  * If you can't delete it, forward it to me, and I'll encourage you
  * At least, *call* the sender and verify the email.

## Passwords

- Use a password manager, like KeePass*
- Create a different, ridonkulous password for everything
- If you have to transfer a secret, use OneTimeSecret
- For the few passwords you can't paste in:
  * easy to remember
  * easy to type
  * long and hard to guess
- Don't:
  * share passwords more than necessary
  * store secrets on shared media in the clear. Move them off or encrypt them!
  * show passwords on screen. Hide them!

## Physical Device Security

* Laptop, phone, watch, other smart device
* Encrypt your storage. It's usually easy at install time.
* If you can't encrypt your storage, maintain better physical control.
* Lock your screen every time you walk away or look away.
